{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "variable.hostname" -}}
{{ (split ":" (include "variable.splithost" . ))._0 }}
{{- end -}}

{{- define "variable.port" -}}
{{ default 22 (split ":" (include "variable.splithost" . ))._1 }}
{{- end -}}

{{- define "variable.username" -}}
{{ (split ":" (include "variable.splitcredentials" . ))._0 }}
{{- end -}}

{{- define "variable.password" -}}
{{ (split ":" (include "variable.splitcredentials" . ))._1 }}
{{- end -}}

{{- define "variable.splitcredentials" -}}
{{- (split "@" .Values.ssh)._0 -}}
{{- end -}}
{{- define "variable.splithost" -}}
{{- (split "@" .Values.ssh)._1 -}}
{{- end -}}